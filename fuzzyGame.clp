;; Variables linguisticas ;;

(deftemplate salud_principal 0 100 puntos_de_vida_principal
(
    (cercanaMuerte (0 1) (50 0))
    (buena         (15 0) (50 1) (85 0))
    (excelente     (50 0) (100 1))
))

(deftemplate salud_enemigo 0 100 puntos_de_vida_enemigo
(
    (cercanaMuerte (0 1) (50 0))
    (buena         (15 0) (50 1) (85 0))
    (excelente     (50 0) (100 1))
))

(deftemplate fuerza 0 5 puntos_de_fuerza
(
    (salirCorriendo  (0 1) (2 0))
    (ataqueDefensivo (1 0) (2 1) (3 1) (4 0))
    (fueraCombate    (3 0) (4 1))
))


;; Reglas ;;

(deffunction fuzzify (?fztemplate ?value ?delta)

        (bind ?low (get-u-from ?fztemplate))
        (bind ?hi  (get-u-to   ?fztemplate))

        (if (<= ?value ?low)
          then
            (assert-string
              (format nil "(%s (%g 1.0) (%g 0.0))" ?fztemplate ?low ?delta))
          else
            (if (>= ?value ?hi)
              then
                (assert-string
                   (format nil "(%s (%g 0.0) (%g 1.0))"
                               ?fztemplate (- ?hi ?delta) ?hi))
              else
                (assert-string
                   (format nil "(%s (%g 0.0) (%g 1.0) (%g 0.0))"
                               ?fztemplate (max ?low (- ?value ?delta))
                               ?value (min ?hi (+ ?value ?delta)) ))
            )
        )
)

(defrule leer-datos
    (initial-fact)
=>
    (printout t "Introduzca los puntos de salud del jugador principal " crlf)
    (bind ?RSaludPrincipal (read))
    (fuzzify salud_principal ?RSaludPrincipal 0.1) 
    (printout t "Introduzca los puntos de salud del jugador enemigo" crlf)
    (bind ?RSaludEnemigo (read))
    (fuzzify salud_enemigo ?RSaludEnemigo 0.1)
)

(defrule defuzzificar
    (declare (salience -1))
    ?f <- (fuerza ?)
=>
    (bind ?p (maximum-defuzzify ?f))
    (printout t "La cantidad de " (get-u-units ?f) " a aplicar es: " ?p crlf)
)

(defrule regla-1
    (salud_principal cercanaMuerte)
    (salud_enemigo buena)
=>
    (assert (fuerza salirCorriendo))
)


(defrule regla-2
    (salud_principal cercanaMuerte)
    (salud_enemigo excelente)
=>
    (assert (fuerza salirCorriendo))
)

(defrule regla-3
    (salud_enemigo cercanaMuerte)
    (salud_principal buena)
=>
    (assert (fuerza fueraCombate))
)


(defrule regla-4
    (salud_enemigo cercanaMuerte)
    (salud_principal excelente)
=>
    (assert (fuerza fueraCombate))
)

(defrule regla-5
    (salud_enemigo buena)
    (salud_principal excelente)
=>
    (assert (fuerza fueraCombate))
)

;; � En cualquier otro caso realizar ataques defensivos ? ;;

