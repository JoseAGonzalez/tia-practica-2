;; Variables linguisticas ;;

(deftemplate temperatura 0 100 grados
(
    (frio  (0 1) (15 1) (20 0))
	(templado (18 1) (23 1) (35 0))
	(caliente  (35 1) (85 1) (100 1))
))

(deftemplate humedad 0 100 porcentaje
(
    (seco          (0 1) (50 0))
    (humedo        (15 0) (50 1) (85 0))
    (mojado        (65 0) (100 1))
))

(deftemplate velocidad 0 2500 rpm
(
    (bajo  (0 0) (250 1) (500 0))
    (medio (450 1) (800 1) (1500 0))
    (alto  (1450 0) (2500 1))
))


;; Reglas ;;

(deffunction fuzzify (?fztemplate ?value ?delta)

        (bind ?low (get-u-from ?fztemplate))
        (bind ?hi  (get-u-to   ?fztemplate))

        (if (<= ?value ?low)
          then
            (assert-string
              (format nil "(%s (%g 1.0) (%g 0.0))" ?fztemplate ?low ?delta))
          else
            (if (>= ?value ?hi)
              then
                (assert-string
                   (format nil "(%s (%g 0.0) (%g 1.0))"
                               ?fztemplate (- ?hi ?delta) ?hi))
              else
                (assert-string
                   (format nil "(%s (%g 0.0) (%g 1.0) (%g 0.0))"
                               ?fztemplate (max ?low (- ?value ?delta))
                               ?value (min ?hi (+ ?value ?delta)) ))
            )
        )
)

(defrule leer-datos
    (initial-fact)
=>
    (printout t "Introduzca la temperatura actual " crlf)
    (bind ?Temperatura (read))
    (fuzzify temperatura ?Temperatura 0.1) 
    (printout t "Introduzca la humedad actual" crlf)
    (bind ?Humedad (read))
    (fuzzify humedad ?Humedad 0.1)
)

(defrule defuzzificar
    (declare (salience -1))
    ?f <- (velocidad ?)
=>
    (bind ?p (maximum-defuzzify ?f))
    (printout t "La cantidad de " (get-u-units ?f) " a aplicar es: " ?p crlf)
)

(defrule regla-1
    (temperatura frio)
    (humedad seco)
=>
    (assert (velocidad medio))
)


(defrule regla-2
    (temperatura frio)
    (humedad humedo)
=>
    (assert (velocidad alto))
)

(defrule regla-3
    (temperatura frio)
    (humedad mojado)
=>
    (assert (velocidad alto))
)


(defrule regla-4
    (temperatura templado)
    (humedad seco)
=>
    (assert (velocidad bajo))
)

(defrule regla-5
    (temperatura templado)
    (humedad humedo)
=>
    (assert (velocidad medio))
)

(defrule regla-6
    (temperatura templado)
    (humedad mojado)
=>
    (assert (velocidad alto))
)

(defrule regla-7
    (temperatura caliente)
    (humedad seco)
=>
    (assert (velocidad medio))
)

(defrule regla-8
    (temperatura caliente)
    (humedad humedo)
=>
    (assert (velocidad alto))
)

(defrule regla-9
    (temperatura caliente)
    (humedad mojado)
=>
    (assert (velocidad alto))
)
;; � En cualquier otro caso realizar ataques defensivos ? ;;

