;; Variables linguisticas ;;

(deftemplate temperatura_actual 0 100 grados
(
	(baja  (0 1) (15 1) (35 1) (40 0))
	(media (35 0) (40 1) (60 1) (70 1))
	(alta  (60 1) (85 1) (100 1))
))


(deftemplate temperatura_anterior 0 100 grados
(
	(baja  (0 1) (15 1) (35 1) (40 0))
	(media (35 0) (40 1) (60 1) (70 1))
	(alta  (60 1) (85 1) (100 1))
))


(deftemplate temperatura_salida 0 100 grados
(
	(OFF (0 1) (15 1) (25 0))
	(F   (30 1) (60 1))
	(C   (20  1) (25 1))
	(MF  (75 1) (100 1))
	(MC  (0 1) (10 1))
))

;; Reglas ;;

(defrule regla-1
    (temperatura_actual alta)
    (temperatura_anterior alta)
=>
	(assert (temperatura_salida MF))
)

(defrule regla-2
    (temperatura_actual media)
    (temperatura_anterior alta)
=>
	(assert (temperatura_salida F))
)

(defrule regla-3
    (temperatura_actual baja)
    (temperatura_anterior alta)
=>
	(assert (temperatura_salida MC))
)

(defrule regla-4
    (temperatura_actual alta)
    (temperatura_anterior media)
=>
	(assert (temperatura_salida MF))
)

(defrule regla-5
    (temperatura_actual media)
    (temperatura_anterior media)
=>
	(assert (temperatura_salida OFF))
)

(defrule regla-6
    (temperatura_actual media)
    (temperatura_anterior baja)
=>
	(assert (temperatura_salida C))
)

(defrule regla-7
    (temperatura_actual baja)
    (temperatura_anterior alta)
=>
	(assert (temperatura_salida MC))
)

(defrule regla-8
    (temperatura_actual baja)
    (temperatura_anterior media)
=>
	(assert (temperatura_salida MC))
)
(defrule regla-9
    (temperatura_actual baja)
    (temperatura_anterior baja)
=>
	(assert (temperatura_salida MC))
)


;; Defusificacion ;;

(deffunction fuzzify (?fztemplate ?value ?delta)

        (bind ?low (get-u-from ?fztemplate))
        (bind ?hi  (get-u-to   ?fztemplate))

        (if (<= ?value ?low)
          then
            (assert-string
              (format nil "(%s (%g 1.0) (%g 0.0))" ?fztemplate ?low ?delta))
          else
            (if (>= ?value ?hi)
              then
                (assert-string
                   (format nil "(%s (%g 0.0) (%g 1.0))"
                               ?fztemplate (- ?hi ?delta) ?hi))
              else
                (assert-string
                   (format nil "(%s (%g 0.0) (%g 1.0) (%g 0.0))"
                               ?fztemplate (max ?low (- ?value ?delta))
                               ?value (min ?hi (+ ?value ?delta)) ))
            )
        )
)

(defrule leer-datos
    (initial-fact)
=>
    (printout t "Introduzca la temperatura actual " crlf)
    (bind ?TActual (read))
    (fuzzify temperatura_actual ?TActual 0.1) 
    (printout t "Introduzca la temperatura anterior" crlf)
    (bind ?TAnterior (read))
    (fuzzify temperatura_anterior ?TAnterior 0.1)
)

(defrule defuzzificar
    (declare (salience -1))
    ?f <- (temperatura_salida ?)
=>
    (bind ?p (maximum-defuzzify ?f))
    (printout t "La temperatura es: " ?p crlf)
)
