;; Variables linguisticas ;;

(deftemplate distancia 0 50 metros
( 
    (cerca (0 1) (15 0))
    (medio (10 0) (25 1) (35 1) (40 0))
    (lejos (35 0) (50 1))
))

(deftemplate velocidad -30 30 kmh
(
    (alejando  (-30 1) (0 0))
    (constante (-10 0) (0 1) (10 0))
    (acercando (0 0) (30 1))
))

(deftemplate presion-freno 0 100 porcentaje
(
    (nula  (z 10 25))
    (media (pi 25 65))
    (alta  (s 65 90))
))



;; Reglas ;;

(deffunction fuzzify (?fztemplate ?value ?delta)

        (bind ?low (get-u-from ?fztemplate))
        (bind ?hi  (get-u-to   ?fztemplate))

        (if (<= ?value ?low)
          then
            (assert-string
              (format nil "(%s (%g 1.0) (%g 0.0))" ?fztemplate ?low ?delta))
          else
            (if (>= ?value ?hi)
              then
                (assert-string
                   (format nil "(%s (%g 0.0) (%g 1.0))"
                               ?fztemplate (- ?hi ?delta) ?hi))
              else
                (assert-string
                   (format nil "(%s (%g 0.0) (%g 1.0) (%g 0.0))"
                               ?fztemplate (max ?low (- ?value ?delta))
                               ?value (min ?hi (+ ?value ?delta)) ))
            )
        )
)

(defrule leer-datos
    (initial-fact)
=>
    (printout t "Introduzca la distancia con respecto al veh�culo (en metros) " crlf)
    (bind ?RDistancia (read))
    (fuzzify distancia ?RDistancia 0.1) 
    (printout t "Introduzca la velocidad del veh�culo (en km/h)" crlf)
    (bind ?RVelocidad (read))
    (fuzzify velocidad ?RVelocidad 0.1)
)

(defrule defuzzificar
    (declare (salience -1))
    ?f <- (presion-freno ?)
=>
    (bind ?p (maximum-defuzzify ?f))
    (printout t "El " (get-u-units ?f) " de frenada debe ser: " ?p crlf)
)


(defrule cerca-alejando
    (distancia cerca)
    (velocidad alejando)
=>
    (assert (presion-freno nula))
)

(defrule cerca-constante
    (distancia cerca)
    (velocidad constante)
=>
    (assert (presion-freno media))
)

(defrule cerca-acercando
    (distancia cerca)
    (velocidad acercando)
=>
    (assert (presion-freno alta))
)

(defrule medio-alejando
    (distancia medio)
    (velocidad alejando)
=>
    (assert (presion-freno nula))
)

(defrule medio-constante
    (distancia medio)
    (velocidad constante)
=>
    (assert (presion-freno nula))
)

(defrule medio-acercando
    (distancia medio)
    (velocidad acercando)
=>
    (assert (presion-freno media))
)

(defrule lejos-alejando
    (distancia lejos)
    (velocidad alejando)
=>
    (assert (presion-freno nula))
)

(defrule lejos-constante
    (distancia lejos)
    (velocidad constante)
=>
    (assert (presion-freno nula))
)

(defrule lejos-acercando
    (distancia lejos)
    (velocidad acercando)
=>
    (assert (presion-freno media))
)
